﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace AutismService.Models
{
    public partial class AutismServiceDBContext : DbContext
    {
        public virtual DbSet<Accounts> Accounts { get; set; }
        public virtual DbSet<Donators> Donators { get; set; }
        public virtual DbSet<Events> Events { get; set; }
        public virtual DbSet<Motives> Motives { get; set; }
        public virtual DbSet<Goals> Goals { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
//To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(@"Server=tcp:autismserviceserver.database.windows.net,1433;Initial Catalog=AutismServiceDB;Persist Security Info=False;User ID=jacek;Password=T0gejjjj;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Accounts>(entity =>
            {
                entity.Property(e => e.Email)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Login)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Donators>(entity =>
            {
                entity.Property(e => e.Email)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Surname)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Events>(entity =>
            {
                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Title)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PicUrl)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Link)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Text)
                    .HasMaxLength(2047)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Goals>(entity =>
            {
                
                entity.Property(e => e.Title)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.GoalPicUrl)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                      .HasMaxLength(2047)
                    .IsUnicode(false);

                entity.Property(e => e.TitleTransformed)
                      .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Motives>(entity =>
            {
                entity.Property(e => e.PicUrl)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });
        }
    }
}
