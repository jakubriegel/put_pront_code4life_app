﻿using System;
using System.Collections.Generic;

namespace AutismService.Models
{
    public partial class Donators
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public int? EventId { get; set; }
    }
}
