﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutismService.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AutismService.Controllers
{
    [Route("api/[controller]")]
    public class GetGoalsController : Controller
    {

        private readonly AutismServiceDBContext ctx;

        public GetGoalsController(AutismServiceDBContext ctx)
        {
            this.ctx = ctx;
        }

        [HttpGet("GetGoals")]
        public IEnumerable<Goals> Get()
        {
            using(var db = ctx)
            {
                return db.Goals.ToList();
            }
        }
    }
}
