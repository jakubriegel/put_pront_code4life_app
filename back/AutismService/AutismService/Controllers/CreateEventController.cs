﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutismService.Models;
using AutismService.MailUtility;
using Microsoft.AspNetCore.Hosting;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AutismService.Controllers
{
    [Route("api/[controller]")]
    public class CreateEventController : Controller
    {
        private readonly AutismServiceDBContext ctx;
        private readonly IEmailService emailService;
        private readonly IHostingEnvironment environment;

        public CreateEventController(AutismServiceDBContext ctx, IEmailService emailService, IHostingEnvironment environment)
        {
            this.ctx = ctx;
            this.emailService = emailService;
            this.environment = environment;
        }

        [HttpPost]
        public Events AddEvent([FromBody]Events newEvent)
        {
            
            using (var db = ctx)
            {
                db.Events.Add(newEvent);
                var count = db.SaveChanges();
                bool invite = newEvent.Certificate.HasValue ? newEvent.Certificate.Value : false;
                if(invite)
                {
                    SendMailToOrganizer(db.Accounts.First(x => x.Id == newEvent.AccountId).Email);
                }
                return newEvent;
            }
        }

        private bool SendMailToOrganizer(string email)
        {

            try
            {
                EmailMessage mail = new EmailMessage();
                EmailAddress receiver = new EmailAddress
                {
                    //Address = "blazej.krzyzanek@student.put.poznan.pl",
                    //Address = "rak.wojtek@wp.pl",
                    //Address = "jacgamespam@gmail.com",
                    Address = email,
                    Name = "Organizator"
                };
                mail.ToAddresses.Add(receiver);
                mail.Subject = "Zaproszenie";
                mail.Content = "TEMPLATE\n\nDziękujemy, \nfundacja JIM\n";
                //DO ZMIANY
                mail.AttachementLocation = environment.WebRootPath + "/Zaproszenie.png";
                emailService.Send(mail);
            }
            catch
            {
                return false;
            }



            return true;
        }
    }
}
