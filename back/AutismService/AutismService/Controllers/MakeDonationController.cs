﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutismService.Models;
using AutismService.Models.Results;
using AutismService.MailUtility;
using Microsoft.AspNetCore.Hosting;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AutismService.Controllers
{
    [Route("api/[controller]")]
    public class MakeDonationController : Controller
    {
        
        private readonly AutismServiceDBContext ctx;
        private readonly IEmailService emailService;
        private readonly IHostingEnvironment environment;

        public MakeDonationController(AutismServiceDBContext ctx, IEmailService emailService, IHostingEnvironment environment)
        {
            this.ctx = ctx;
            this.emailService = emailService;
            this.environment = environment;
        }

        [HttpPost]
        public Donators MakeDonaton([FromBody]DonatorsResult donator)
        {
            using (var db = ctx)
            {
                var tempEvent = db.Events.First(x => x.Link == donator.Link);
                donator.EventId = tempEvent.Id;
                var donatorToSave = new Donators
                {
                    Email = donator.Email,
                    EventId = donator.EventId,
                    Name = donator.Name,
                    Surname = donator.Surname
                };
                db.Donators.Add(donatorToSave);

                var eventToEdit = db.Events.First(x => x.Id == donator.EventId);

                eventToEdit.DonationAmount += 1;
                eventToEdit.DonationSum += donator.Amount;

                var motiveToGet = db.Motives.FirstOrDefault(x => x.Id == donator.MotivesId);
                if (motiveToGet == null) motiveToGet = db.Motives.First();


                var count = db.SaveChanges();


                switch (donator.MotivesId)
                {
                    case 1:
                        GenerateCertyficates(donator.Name, donator.Surname, donator.Male,
                                             db.Goals.First(x => x.Id == eventToEdit.GoalId).TitleTransformed,
                                             eventToEdit.Title);
                        break;
                    default:
                        GenerateCertyficates(donator.Name, donator.Surname, donator.Male,
                                             db.Goals.First(x => x.Id == eventToEdit.GoalId).TitleTransformed,
                                             eventToEdit.Title);
                        break;
                }


                SendMail(donator.Email, donator.Name, donator.Surname);

                return donator;
            }
        }

        private bool SendMail(string email, string name, string surname)
        {

            try
            {
                EmailMessage mail = new EmailMessage();
                EmailAddress receiver = new EmailAddress
                {
                    //Address = "blazej.krzyzanek@student.put.poznan.pl",
                    //Address = "rak.wojtek@wp.pl",
                    //Address = "jacgamespam@gmail.com",
                    Address = email,
                    Name = "Darczyńca"
                };
                mail.ToAddresses.Add(receiver);
                mail.Subject = "Certyfikat";
                mail.Content = "Twoja pomoc stanowi pierwsze kroki w stronę lepszego " +
                    "jutra dla tych, którzy go najbardziej potrzebują.\n\nW załączniku znajduje się " +
                    "certyfikat, który jest przeznaczony do wydrukowania i stanowi idealny prezent  " +
                    "dla pary młodej, solenizanta, jubilatów.\n\nDziękujemy, \nfundacja JIM\n";
                //DO ZMIANY
                mail.AttachementLocation = environment.WebRootPath + "/certyfikat_" + name + "_" + surname + ".png";
                emailService.Send(mail);
            }
            catch
            {
                return false;
            }



            return true;
        }




        private bool GenerateCertyficates(string name, string surname, bool? male,
                                          string titleTransformed, string title)
        {
            var filePath = environment.WebRootPath;

            using (var image = Image.FromStream(System.IO.File.OpenRead(filePath + "/" + "certyfikat_bez_tekstu.png")))
            using (var graphics = Graphics.FromImage(image))
            {
                // Do what you want using the Graphics object here.
                var brush = new SolidBrush(ColorTranslator.FromHtml("#214869"));
                StringFormat format = new StringFormat()
                {
                    Alignment = StringAlignment.Center,
                    LineAlignment = StringAlignment.Center
                };
                graphics.SmoothingMode = SmoothingMode.AntiAlias;

                // The interpolation mode determines how intermediate values between two endpoints are calculated.
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;

                // Use this property to specify either higher quality, slower rendering, or lower quality, faster rendering of the contents of this Graphics object.
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                // This one is important
                graphics.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;


                // IMIE NAZWISKO
                var font = new Font("Gill Sans MT", 30);
                RectangleF rect = new RectangleF(0, 1230, image.Width, 400);
                graphics.DrawString(name + " " + surname, font, brush, rect, format);

                String text = "poprzez udzielenie pomocy\n" + titleTransformed + "\npodczas zbiórki zorganizowanej w ramach\nuroczystości jaką jest\n" + title;

                // Otrzyał otrzymała
                font = new Font("Gill Sans MT", 24);
                rect.Y = 1340;
                bool maleNotNull = male.HasValue ? male.Value : true;
                graphics.DrawString(maleNotNull ? "został" : "została", font, brush, rect, format);

                // Mariusza Pudzianowskiego 
                rect = new RectangleF(0, 1380, image.Width, 1400);
                graphics.DrawString(text, font, brush, rect, format);

                // Important part!
                graphics.Flush();
                image.Save(filePath + "/certyfikat_" + name + "_" + surname + ".png");
            }
            return true;
        }




    }
}
