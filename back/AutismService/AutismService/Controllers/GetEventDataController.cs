﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AutismService.Models;
using AutismService.Models.Results;

namespace AutismService.Controllers
{
    [Produces("application/json")]
    [Route("api/GetEventData")]
    public class GetEventDataController : Controller
    {
        private readonly AutismServiceDBContext ctx;

        public GetEventDataController(AutismServiceDBContext ctx)
        {
            this.ctx = ctx;
        }

        [HttpGet("{link}")]
        public EventResult GetEventData(string link)
        {
            using (var db = ctx)
            {
                var eventToGet = db.Events.FirstOrDefault(x => x.Link == link);
                if(eventToGet == null)
                {
                    return new EventResult { Id = -1 };
                }
                var goalToGet = db.Goals.FirstOrDefault(x => x.Id == eventToGet.GoalId);
                if (goalToGet == null)
                {
                    return new EventResult { Id = -1 };
                }
                var eventResult = new EventResult
                {
                    AccountId = eventToGet.AccountId,
                    Certificate = eventToGet.Certificate,
                    Date = eventToGet.Date,
                    DonationAmount = eventToGet.DonationAmount,
                    DonationSum = eventToGet.DonationSum,
                    Id = eventToGet.Id,
                    Link = eventToGet.Link,
                    PicUrl = eventToGet.PicUrl,
                    Text = eventToGet.Text,
                    Title = eventToGet.Title,
                    GoalId = goalToGet.Id,
                    GoalDecription = goalToGet.Description,
                    GoalPicUrl = goalToGet.GoalPicUrl,
                    GoalTitle = goalToGet.Title
                };
                return eventResult;
                //return eventToGet;
            }


        }
    }
}