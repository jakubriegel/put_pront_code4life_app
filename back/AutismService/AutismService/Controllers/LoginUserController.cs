﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutismService.Models;
using AutismService.Models.Results;
using Microsoft.Extensions.Caching.Memory;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AutismService.Controllers
{
    [Produces("application/json")]
    [Route("api/LoginUser")]
    public class LoginUserController : Controller
    {
        private readonly AutismServiceDBContext ctx;
        private readonly IMemoryCache cache;

        public LoginUserController(AutismServiceDBContext ctx, IMemoryCache cache)
        {
            this.ctx = ctx;
            this.cache = cache;
        }

        [HttpPost]
        public LoginResult LoginUser([FromBody] Accounts account)
        {
            using(var db = ctx)
            {
                var acc = db.Accounts.FirstOrDefault(x => x.Email == account.Email && x.Password == Utility.Hash(account.Password));
                if (acc == null) return new LoginResult { AccountId = 0, LoginSuccesful = false };
                var token = GetToken(account.Login);
                var k = new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromMinutes(10));
                cache.Set(account.Email, token, k);
                return new LoginResult
                {
                    AccountId = acc.Id,
                    LoginSuccesful = true,
                    Email = acc.Email,
                    Login = acc.Login,
                    Token = token
                };

            }
        }

        private static string GetToken(string seed)
        {
            return Utility.Hash(seed + DateTime.Now.ToBinary());
        }
    }
}
