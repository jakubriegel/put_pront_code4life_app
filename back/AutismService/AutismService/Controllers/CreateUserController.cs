﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutismService.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AutismService.Controllers
{
    [Route("api/[controller]")]
    public class CreateUserController : Controller
    {
        private readonly AutismServiceDBContext ctx;

        public CreateUserController(AutismServiceDBContext ctx)
        {
            this.ctx = ctx;
        }

        [HttpPost]
        public Accounts AddAccount([FromBody]Accounts account)
        {
            using (var db = ctx)
            {
                account.Password = Utility.Hash(account.Password);
                db.Accounts.Add(account);
                var count = db.SaveChanges();
                return account;
            }
        }
    }
}
