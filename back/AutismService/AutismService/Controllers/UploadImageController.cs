﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using AutismService.Models.UploadModels;
using Microsoft.AspNetCore.Http;
using System.Net.Http.Headers;
using System.IO;
using Microsoft.AspNetCore.Cors;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AutismService.Controllers
{
    [Route("api/[controller]")]
    public class UploadImageController : Controller
    {
        private Account _account;
        public UploadImageController(Account account)
        {
            _account = account;
        }

        [HttpPost]
        [EnableCors("AllowAllOrigins")]
        public IActionResult Upload(IFormFile file)
        {
            var filename = ContentDispositionHeaderValue
                            .Parse(file.ContentDisposition)
                            .FileName
                            .Trim('"');
            using (Stream stream = file.OpenReadStream())
            {
                var bg = new BackgroundUploader( _account, filename, stream);
                var imgs = bg.Upload();
                if (imgs != null) return Ok(imgs);
                else return Ok("Cannot upload");
            }
        }
    }

    public class BackgroundUploader
    {
        private Cloudinary m_cloudinary;
        private List<ImageUploadParamsExt> m_uploadParams;
        private List<Image> m_images;

        public List<Image> Images { get { return m_images; } }

        public Api CloudinaryApi { get { return m_cloudinary.Api; } }

        public BackgroundUploader( Account acc, string filename, Stream stream)
        {
            m_cloudinary = new Cloudinary(acc);

            m_uploadParams = new List<ImageUploadParamsExt>();

            m_uploadParams.Add(new ImageUploadParamsExt()
            {
                File = new FileDescription(filename, stream),
                Tags = "basic_mvc4",

                Caption = "Local file, Fill 200x150",
            });

            m_images = new List<Image>();
        }

        public List<Image> Upload()
        {
            //to do pararallel
            try
            {
                for (int i = 0; i < m_uploadParams.Count; i++)
                {
                    // Using Cloudinary API to upload images
                    ImageUploadResult result = m_cloudinary.Upload(m_uploadParams[i]);

                    m_images.Add(new Image()
                    {
                        Caption = m_uploadParams[i].Caption,
                        PublicId = result.PublicId,
                        Url = result.Uri.ToString(),
                        Format = result.Format
                    });

                }

                return m_images;
            }
            catch
            {
                return null;
            }
        }
    }
}
