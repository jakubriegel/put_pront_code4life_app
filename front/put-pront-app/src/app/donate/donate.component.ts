import { Component, OnInit, ElementRef, OnDestroy } from '@angular/core';
import { Donate } from '../donate';
import { ApiService } from '../services/api.service';
import { ActivatedRoute } from '@angular/router';
import { ViewChild } from '@angular/core';

@Component({
  selector: 'app-donate',
  templateUrl: './donate.component.html',
  styleUrls: ['./donate.component.css']
})
export class DonateComponent implements OnInit, OnDestroy {
  public sub: any;
  public model = new Donate('', '', '', '', 10, 1);
  public motives: Motive[];

  @ViewChild('donateFormElement') formVC: ElementRef;
  form: HTMLFormElement;

  @ViewChild('paymentHolder') paymentHolderVC: ElementRef;
  paymentHolder: HTMLDivElement;

  @ViewChild('confirmationHolder') confirmationHolderVC: ElementRef;
  confirmationHolder: HTMLDivElement;

  constructor(private api: ApiService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.api.getMotives().subscribe(res => {
      this.motives = <Motive[]>res;
    });

    this.sub = this.route.params.subscribe(params => {
      this.model.Link = params['id'];
    });

    this.form = <HTMLFormElement> this.formVC.nativeElement;
    this.paymentHolder = <HTMLDivElement> this.paymentHolderVC.nativeElement;
    this.paymentHolder.style.display = 'none';
    this.confirmationHolder = <HTMLDivElement> this.confirmationHolderVC.nativeElement;
    this.confirmationHolder.style.display = 'none';
  }

  onSubmit() {
    this.form.style.display = 'none';
    this.paymentHolder.style.display = 'block';

    this.processPayment();
  }

  private processPayment(): void {
    this.api.makePayment().subscribe(r => { this.finalizeDonation(); });
  }

  private finalizeDonation(): void {
    this.paymentHolder.style.display = 'none';
    this.confirmationHolder.style.display = 'block';

    this.api.makeDonation(this.model).subscribe((r) => console.log(r));
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}

interface Motive {
  id: number;
  picUrl: string;
}
