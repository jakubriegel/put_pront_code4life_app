import {Router} from '@angular/router';

export class LoginUtils {
    static proceedIfLogged(toDo, router: Router) {
        if (localStorage.getItem('logged') === 'true') {
            toDo();
        } else {
            router.navigate(['login']);
        }
    }
}
