import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-event-page',
  templateUrl: './event-page.component.html',
  styleUrls: ['./event-page.component.css']
})
export class EventPageComponent implements OnInit {
  constructor(
    private service: ApiService,
    private route: ActivatedRoute,
    private router: Router
  ) {}
  public event: CharityEvent;
  private sub: any;
  private link: string;

  ngOnInit() {
      this.sub = this.route.params.subscribe(params => {
        this.getEventByPath(params['id']);
      });
  }

  getEventByPath(path: string) {
    this.service.getEventByPath(path).subscribe(res => {
      this.event = <CharityEvent>res;

      // id -1 means that the page does not exist
      if (this.event.id === -1) {
        this.router.navigate(['/not-found']);
      }
    });
  }
}

interface CharityEvent {
  accountId: number;
  certificate: boolean;
  date: any;
  donationAmount: number;
  donationSum: number;
  goalDecription: string;
  goalId: number;
  goalPicUrl: string;
  goalTitle: string;
  id: number;
  link: string;
  picUrl: string;
  text: string;
  title: string;
}
