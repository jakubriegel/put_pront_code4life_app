export interface LoginUserRequest {
    email: string;
    password: string;
}

export interface LoginUserResponse {
    loginSuccesful: boolean;
    accountId: number;
    email: string;
    login: string;
    token: string;
}

export interface CreateUserRequest {
    email: string;
    login: string;
    password: string;
}

export interface CreateEvetRequest {
    title: string;
    date: string;
    text: string;
    picUrl: string;
    certificate: boolean;
    donationSum: number;
    donationAmount: number;
    accountId: number;
    link: string;
    goalId: number;
}

export interface Goal {
    id: number;
    title: string;
    titleTransformed: string;
    goalPicUrl: string;
    description: string;
}
