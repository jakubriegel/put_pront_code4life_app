import { Component, OnInit } from '@angular/core';

import { User } from './../user';
import { ApiService } from '../services/api.service';
import { LoginUserResponse } from '../apiData';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  model = new User('', '');

  constructor(private api: ApiService, private router: Router) { }

  ngOnInit() {
  }

  onSubmit() {
    this.api.loginUser(this.model.email, this.model.password).subscribe(r => this.login(r));
  }

  login(data: LoginUserResponse) {
    if (data.loginSuccesful) {
      localStorage.setItem('logged', 'true');
      localStorage.setItem('name', data.login);
      localStorage.setItem('email', data.email);
      localStorage.setItem('id', data.accountId.toString());
      localStorage.setItem('token', data.token);

      this.router.navigate(['/profile']);

    } else {
      localStorage.setItem('logged', 'false');
      alert('błędne dane logowania');
    }
  }

}
