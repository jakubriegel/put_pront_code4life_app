export class Donate {
  constructor(
    public email: string,
    public Link: string,
    public name: string,
    public surname: string,
    public amount: number,
    public MotivesId: number,
    public male: boolean = true
  ) {}
}
